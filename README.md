# Güvenli İnternet
Güvenli internet sizi izlemeyen, reklamsız, kötü amaçlı site barındırmayan bir internet modelidir. Bu modele hiçbir zaman tamamen ulaşma imkanımız olmasada olabildiğince yaklaşabilmek için bu proje var.

Anti-adblockerdan etkilenmez. Tarayıcı eklentisi olmadığı için tüm sistemde reklam engellemesi yapar. 


# Kurulumu:


1-hosts dosyasını /etc/hosts dosyası ile değiştirin. 

2-Tarayıcınızı kapatıp açın

3-Güvenli internetinizin tadını çıkarın

Gözden kaçmış bir reklam-bot-pislik varsa Telegram üzerinden @parduscix olarak bana ulaşabilirsiniz. veya Pull Request gönderebilirsiniz...


# Alternatif kurulum:

root yetkisi alın ve aşağıdaki tek satır kodu çalıştırın


wget https://gitlab.com/parduscix/Guvenli_Internet/raw/master/hosts hosts ; mv -f hosts /etc/hosts

# Android cihaza kurulum:

Adaway listelerine şunu ekleyebilirsiniz:
https://gitlab.com/parduscix/Guvenli_Internet/raw/master/hosts

Veya /d/gapps uygulamasının adblocker kısmını kullanabilirsiniz:
https://f-droid.org/en/packages/org.droidtr.deletegapps/

